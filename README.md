# markdown-by-example

I have seen several Markdown tutorials and I believe I can write a better one.

Design principles:
1. Show, don't tell
1. Interactive
2. Less is more
3. Use globally relevant examples to help translators

# License

I have not decided yet, but I will soon.